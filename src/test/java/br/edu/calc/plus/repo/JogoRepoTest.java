package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@ActiveProfiles("test")
public class JogoRepoTest {

    @Autowired
    private JogoRepo jogoRepo;

    @Autowired
    private UsuarioRepo usuarioRepo;

    @Autowired
    private PartidaRepo partidaRepo;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    public void inicializar() {
        System.out.println("----------------- CRIANDO BASE DE DADOS -----------------");
        Usuario u1 = new Usuario(1, "Chapolin", "chapolin", "123456", "chapolin@chaves.com", "Acapulco", LocalDate.of(1980, Month.MARCH, 10));
        usuarioRepo.save(u1);

        Partida partida1 = new Partida(1, LocalDateTime.now(), 0, 0);
        partida1.setUsuario(u1);
        partidaRepo.save(partida1);

        Jogo jogo1 = new Jogo(5, 10, 30, EOperator.soma, 50, 35, 0);
        jogo1.setPartida(partida1);
        jogoRepo.save(jogo1);

        Jogo jogo2 = new Jogo(8, 20, 40, EOperator.subtracao, 250, 350, 0);
        jogo2.setPartida(partida1);
        jogoRepo.save(jogo2);

        Jogo jogo3 = new Jogo(9, 30, 60, EOperator.soma, 50, 50, 0);
        jogo3.setPartida(partida1);
        jogoRepo.save(jogo3);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaGetAllErros() {

        // Executar o método que está sendo testado
        long count = jogoRepo.getAllErros();

        // Verificar o resultado
        assertEquals(2, count);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaGetAllAcertos() {

        // Executar o método que está sendo testado
        long count = jogoRepo.getAllAcertos();

        // Verificar o resultado
        assertEquals(1, count);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaGetAcertosDia() {

        // Executar o método que está sendo testado
        long count = jogoRepo.getAcertosDia(LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(1));

        // Verificar o resultado
        assertEquals(1, count);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaGetErrosDia() {

        // Executar o método que está sendo testado
        long count = jogoRepo.getErrosDia(LocalDateTime.now().minusDays(1), LocalDateTime.now().plusDays(1));

        // Verificar o resultado
        assertEquals(2, count);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaGetAllAcertosUser() {

        // Executar o método que está sendo testado
        long count = jogoRepo.getAllAcertosUser(1);

        // Verificar o resultado
        assertEquals(1, count);
    }

}
