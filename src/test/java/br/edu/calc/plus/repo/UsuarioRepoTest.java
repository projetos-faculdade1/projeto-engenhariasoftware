package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@ActiveProfiles("test")
public class UsuarioRepoTest {

    @Autowired
    private UsuarioRepo usuarioRepo;

    @BeforeEach
    public void inicializar() {
        System.out.println("----------------- CRIANDO BASE DE DADOS -----------------");
        Usuario u1 = new Usuario(1, "José da Silva", "josedasilva", "123456", "jose@chaves.com", "Guarará", LocalDate.of(1980, Month.MARCH, 10));
        usuarioRepo.save(u1);

        Usuario u2 = new Usuario(3,"Lucas Alves", "lucasalves4", "lucasalves@email.com", "147258", "Juiz de Fora", LocalDate.of(1995, Month.APRIL, 12));
        usuarioRepo.save(u2);
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaFindByName(){

        // Executar o método que está sendo testado
        Optional<Usuario> optionalUsuario = usuarioRepo.findByNome("Lucas Alves");

        // Verificar o resultado
        assertTrue(optionalUsuario.isPresent());
        assertEquals("Lucas Alves", optionalUsuario.get().getNome());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaFindByLogin(){

        // Executar o método que está sendo testado
        Optional<Usuario> optionalUsuario = usuarioRepo.findByLogin("josedasilva");

        // Verificar o resultado
        assertTrue(optionalUsuario.isPresent());
        assertEquals("josedasilva", optionalUsuario.get().getLogin());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void testaFindById(){

        // Executar o método que está sendo testado
        Optional<Usuario> optionalUsuario = usuarioRepo.findById(1);

        // Verificar o resultado
        assertTrue(optionalUsuario.isPresent());
        assertEquals(1, optionalUsuario.get().getId());
    }
}
