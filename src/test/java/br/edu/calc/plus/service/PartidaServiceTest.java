package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.EOperator;
import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.domain.dto.JogoListDTO;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.repo.PartidaRepo;
import br.edu.calc.plus.repo.UsuarioRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PartidaServiceTest {

    @Mock
    private PartidaRepo pDao;

    @Mock
    private JogoRepo jDao;

    @Mock
    private UsuarioRepo uDao;

    @Mock
    private JogoService jogoService;

    @InjectMocks
    private PartidaService partidaService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testaGetPremioUsers() {
        // Cenário
        Mockito.when(pDao.getAllBonus()).thenReturn(100.0);

        // Executar
        String resultado = partidaService.getPremioUsers();

        // Verificar
        Assertions.assertEquals("Cr$ 100.00", resultado);
    }

    @Test
    void testaIniciarPartida() throws Exception {
        // Cenário
        int idUser = 1;
        Partida partidaSalva = new Partida(1, LocalDateTime.now(), 0, 0);
        List<Jogo> jogos = Arrays.asList(
                new Jogo(null, 10, 20, EOperator.soma, 30, -1111, 0),
                new Jogo(null, 5, 15, EOperator.subtracao, -10, -1111, 0),
                new Jogo(null, 8, 7, EOperator.multiplicacao, 56, -1111, 0)
        );
        Mockito.when(uDao.getById(idUser)).thenReturn(new Usuario());
        Mockito.when(pDao.save(Mockito.any(Partida.class))).thenAnswer(invocation -> {
            Partida partida = invocation.getArgument(0);
            partida.setId(partidaSalva.getId());
            return partida;
        });
        Mockito.when(jogoService.criarJogosAleatorio(Mockito.anyInt(), Mockito.anyInt())).thenReturn(jogos);
        Mockito.when(jDao.save(Mockito.any(Jogo.class))).thenReturn(new Jogo());

        // Executar
        Partida resultado = partidaService.iniciarPartida(idUser);

        // Verificar
        Assertions.assertEquals(partidaSalva, resultado);
        Assertions.assertEquals(jogos, resultado.getJogoList());
        Mockito.verify(pDao, Mockito.times(2)).save(Mockito.any(Partida.class));
        Mockito.verify(jDao, Mockito.times(3)).save(Mockito.any(Jogo.class));
    }

    @Test
    void testaSavePartida() throws Exception {
        // Cenário
        int idPartida = 1;
        int idUser = 1;
        int posicao = 1;
        int idJogo = 1;
        double valor = 10.0;

        List<Jogo> jogos = Arrays.asList(
                new Jogo(null, 10, 20, EOperator.soma, 30, -1111, 0),
                new Jogo(null, 5, 15, EOperator.subtracao, -10, -1111, 0),
                new Jogo(null, 8, 7, EOperator.multiplicacao, 56, -1111, 0)
        );


        Partida partida = new Partida(1, LocalDateTime.now(), 0, 0);
        partida.setJogoList(jogos);

        Jogo jogo = new Jogo(null, 10, 20, EOperator.soma, 30, 10, 0);

        Mockito.when(pDao.findByIdAndUsuarioId(idPartida, idUser)).thenReturn(partida);
        Mockito.when(jDao.save(Mockito.any(Jogo.class))).thenReturn(jogo);

        // Executar
        Partida resultado = partidaService.savePartida(idPartida, idUser, posicao, idJogo, valor);

        // Verificar
        Assertions.assertEquals(partida, resultado);
        Assertions.assertEquals(valor, jogo.getResposta());
        Mockito.verify(jDao).save(Mockito.any(Jogo.class));
        Mockito.verify(pDao).save(Mockito.any(Partida.class));
    }
}