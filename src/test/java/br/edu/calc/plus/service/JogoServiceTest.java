package br.edu.calc.plus.service;

import br.edu.calc.plus.repo.JogoRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;


public class JogoServiceTest {

    @Mock
    private JogoRepo jDao;

    @InjectMocks
    private JogoService jogoService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testaGetAcertosUltimos10Dias() {
        // Cenário
        Mockito.when(jDao.getAcertosDia(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class)))
                .thenReturn(10L);

        // Executar
        long[] result = jogoService.getAcertosUltimos10Dias();

        // Verificar
        Assertions.assertArrayEquals(new long[]{10L, 10L, 10L, 10L, 10L, 10L, 10L, 10L, 10L, 10L}, result);
    }

    @Test
    void testaGetErrosUltimos10Dias() {
        // Cenário
        Mockito.when(jDao.getErrosDia(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class)))
                .thenReturn(5L);

        // Executar
        long[] result = jogoService.getErrosUltimos10Dias();

        // Verificar
        Assertions.assertArrayEquals(new long[]{5L, 5L, 5L, 5L, 5L, 5L, 5L, 5L, 5L, 5L}, result);
    }

    @Test
    void testaBonusUsuario() {
        // Cenário
        int id1 = 1;
        int id2 = 2;
        int id3 = 3;
        int id4 = 4;
        int id5 = 5;
        Mockito.when(jDao.getAllAcertosUser(id1)).thenReturn(5L);
        Mockito.when(jDao.getAllAcertosUser(id2)).thenReturn(20L);
        Mockito.when(jDao.getAllAcertosUser(id3)).thenReturn(80L);
        Mockito.when(jDao.getAllAcertosUser(id4)).thenReturn(120L);
        Mockito.when(jDao.getAllAcertosUser(id5)).thenReturn(180L);

        // Executar
        double result1 = jogoService.bonusUsuario(id1);
        double result2 = jogoService.bonusUsuario(id2);
        double result3 = jogoService.bonusUsuario(id3);
        double result4 = jogoService.bonusUsuario(id4);
        double result5 = jogoService.bonusUsuario(id5);

        // Verificar
        Assertions.assertEquals(0, result1);
        Assertions.assertEquals(0.1, result2);
        Assertions.assertEquals(0.2, result3);
        Assertions.assertEquals(0.3, result4);
        Assertions.assertEquals(0.5, result5);
    }


}
