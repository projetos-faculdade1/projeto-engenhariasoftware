package br.edu.calc.plus.domain.dto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class JogoListDTOTest {

    @Test
    void testaDataFormatada() {
        // Cenário
        LocalDateTime dataJogo = LocalDateTime.of(2023, 7, 13, 10, 30);
        JogoListDTO jogo = new JogoListDTO();
        jogo.setDataJogo(dataJogo);

        // Executar
        String dataFormatada = jogo.getDataFormatada();

        // Verificar
        Assertions.assertEquals("13/07/2023 10:30:00", dataFormatada);
    }
}
